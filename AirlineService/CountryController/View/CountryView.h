//
//  CountryView.h
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryView : UIView

@property (nonatomic, strong) UITableView *countryTable;
@property (nonatomic, strong) UISearchBar *searchBarCountry;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property (nonatomic, assign) CGFloat topOffset;

@end
