//
//  CountryViewController.m
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "CountryViewController.h"
#import "CountryView.h"
#import "AirportsViewController.h"

#import <CoreLocation/CoreLocation.h>
#import "CoreData/CoreData.h"

#import "AppDelegate.h"

#import "Countries+CoreDataClass.h"
#import "Cities+CoreDataClass.h"


@interface CountryViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, CLLocationManagerDelegate>

@property (nonatomic,strong) CountryView *countryView;
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSPersistentContainer *persistentContainer;
@property (nonatomic, strong) NSArray *searchResult;

@end

@implementation CountryViewController

-(void)loadView{
    self.countryView = [[CountryView alloc] init];
    _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    self.view = self.countryView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
     NSManagedObjectContext *context = self.persistentContainer.viewContext;
     NSFetchRequest *request;
    
    if (self.typeOfController == CountryViewControllerTypeCountry){
        self.navigationItem.title = @"Выберите страну";
        request = [NSFetchRequest fetchRequestWithEntityName:@"Countries"];
    }
    else
    {
        self.navigationItem.title = @"Выберите город";
        request = [NSFetchRequest fetchRequestWithEntityName:@"Cities"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"countryCode == %@",self.countryCode];
        request.predicate = predicate;
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [request setSortDescriptors:@[sort]];

    self.fetchedResultsController = [[NSFetchedResultsController alloc]
                                              initWithFetchRequest:request
                                              managedObjectContext:context
                                              sectionNameKeyPath:nil
                                              cacheName:nil];
    [self.fetchedResultsController performFetch:nil];
    self.searchResult = self.fetchedResultsController.fetchedObjects;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"location"] style:UIBarButtonItemStyleDone target:self action:@selector(getLocation:)];
    
    self.countryView.countryTable.dataSource = self;
    self.countryView.countryTable.delegate = self;
    
    self.countryView.searchBarCountry.delegate = self;

}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    [self.locationManager stopUpdatingLocation];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.countryView.countryTable selectRowAtIndexPath:nil
                                               animated:NO
                                         scrollPosition:UITableViewScrollPositionTop];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.searchResult count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"MYCell"];
    }
    
        if (self.typeOfController == CountryViewControllerTypeCountry) {
            Countries *cellObject = [self.searchResult objectAtIndex:indexPath.row];
            cell.textLabel.text = cellObject.name;
            cell.detailTextLabel.hidden = YES;
            cell.detailTextLabel.text = cellObject.code;
        }
        else {
            Cities *cellObject = [self.searchResult objectAtIndex:indexPath.row];
            cell.textLabel.text = cellObject.name;
            cell.detailTextLabel.hidden = YES;
            cell.detailTextLabel.text = cellObject.code;
        }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.typeOfController == CountryViewControllerTypeCountry)
    {
        CountryViewController *cityViewController = [[CountryViewController alloc] init];
        cityViewController.typeOfController = CountryViewControllerTypeCity;
        cityViewController.countryCode = [tableView cellForRowAtIndexPath:indexPath].detailTextLabel.text;
        [self.navigationController pushViewController:cityViewController animated:YES];

    }
    else {
        AirportsViewController *airportsViewController = [[AirportsViewController alloc] init];
        airportsViewController.delegate = self.navigationController.viewControllers[0];
        airportsViewController.cityCodeForSearch = [tableView cellForRowAtIndexPath:indexPath].detailTextLabel.text;
        [self.navigationController pushViewController:airportsViewController animated:YES];
    }
}

- (void)searchForText:(NSString*)searchText {
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[cd] %@",searchText];
    self.searchResult = [self.fetchedResultsController.fetchedObjects filteredArrayUsingPredicate:resultPredicate];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.view endEditing:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self searchForText:searchText];
    [self.countryView.countryTable reloadData];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    if ([searchBar.text isEqualToString:@""]) self.searchResult=nil;
    else  [self searchForText:searchBar.text];
    [searchBar setShowsCancelButton:YES animated:YES];
    [self.countryView.countryTable reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.searchResult = self.fetchedResultsController.fetchedObjects;
    self.countryView.searchBarCountry.text = @"";
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
    [self.countryView.countryTable reloadData];
}

- (void)getLocation:(UIBarButtonItem *)sender {
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [self.countryView.indicatorView startAnimating];
    
    [self.locationManager startUpdatingLocation];
    
    CLGeocoder *geo = [[CLGeocoder alloc] init];
    
    [geo reverseGeocodeLocation:self.locationManager.location completionHandler:^(NSArray* placemarks, NSError* error){
        if ([placemarks count] > 0)
        {
            CLPlacemark *city= [placemarks objectAtIndex:0];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:nil
                                             message:[NSString stringWithFormat:@"Ваш город %@?",city.locality]
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction
                                           actionWithTitle:@"Да"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               AirportsViewController *airportsViewController = [[AirportsViewController alloc] init];
                                               airportsViewController.delegate = self.navigationController.viewControllers[0];
                                               airportsViewController.localtionSearch = YES;
                                               airportsViewController.latitude = [NSNumber numberWithDouble:city.location.coordinate.latitude];
                                               airportsViewController.longitude = [NSNumber numberWithDouble:city.location.coordinate.longitude];
                                               airportsViewController.cityCodeForSearch = city.locality;
                                               [self.navigationController pushViewController:airportsViewController animated:YES];
                                           }];
                
                [alert addAction:yesButton];
                
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"Нет"
                                           style:UIAlertActionStyleDefault
                                           handler:nil];
                
                [alert addAction:noButton];

                [self presentViewController:alert animated:YES completion:nil];

            });
        }
        [self.countryView.indicatorView stopAnimating];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }];
    

}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.countryView.topOffset = self.topLayoutGuide.length;
}


@end
