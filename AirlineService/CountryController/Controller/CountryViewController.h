//
//  CountryViewController.h
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    CountryViewControllerTypeCountry,
    CountryViewControllerTypeCity,
} CountryViewControllerType;

@interface CountryViewController : UIViewController

@property (nonatomic, assign) CountryViewControllerType typeOfController;
@property (nonatomic,strong) NSString *countryCode;

@end
