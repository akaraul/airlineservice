//
//  AirportsViewController.h
//  AirlineService
//
//  Created by Андрей on 20.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  AirportsViewControllerDelegate;

@interface AirportsViewController : UIViewController

@property (nonatomic, strong) NSString *cityCodeForSearch;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, assign) BOOL localtionSearch;
@property (nonatomic, weak) id<AirportsViewControllerDelegate> delegate;

@end

@protocol AirportsViewControllerDelegate <NSObject>

- (void)setAirport:(NSString *)airport andCode:(NSString *)code;

@end
