//
//  AirportsViewController.m
//  AirlineService
//
//  Created by Андрей on 20.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "AirportsViewController.h"
#import "AirportsView.h"
#import "AirportsModel.h"
#import "Airports+CoreDataClass.h"

@interface AirportsViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic,strong) AirportsView *airportsView;
@property (nonatomic,strong) AirportsModel *airportsModel;

@end

@implementation AirportsViewController

- (void)loadView {
    self.airportsView = [[AirportsView alloc] init];
    self.view = self.airportsView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.airportsModel = [[AirportsModel alloc] init];
    
    if (self.localtionSearch) {
        self.cityCodeForSearch = [self.airportsModel getCityCodeByCityName:self.cityCodeForSearch];
    }
    
    [self.airportsModel getAirportsByCityCode:self.cityCodeForSearch];
    self.navigationItem.title = @"Выберите аэропорт";
    
    
    if ([self.airportsModel.airports count] == 0) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"В выбранном городе нет аэропортов!\nБудут показаны ближайшие"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                   actionWithTitle:@"ОК"
                                   style:UIAlertActionStyleDefault
                                   handler:nil];
        
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
        [self.airportsView.indicatorView startAnimating];
        
        //if (!self.localtionSearch)
        
        [self.airportsModel getNearAirportsByCoordinateWithLatitude:self.latitude andLongitude:self.longitude withComplition:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.airportsView.indicatorView stopAnimating];
                [self.airportsView.airportsTable reloadData];
            });
        }];
    }
    
    self.airportsView.airportsTable.dataSource = self;
    self.airportsView.airportsTable.delegate = self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.airportsModel.airports count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"MYCell"];
    }
    Airports *airport = [self.airportsModel.airports objectAtIndex:indexPath.row];
    cell.textLabel.text = airport.name;
    if (self.localtionSearch) {
        CLLocationCoordinate2D origin = CLLocationCoordinate2DMake([self.latitude doubleValue], [self.longitude doubleValue]);
        CLLocationCoordinate2D destination = CLLocationCoordinate2DMake(airport.latitude, airport.longitude);
        cell.detailTextLabel.text = [self.airportsModel getDistanceWithOriginCoordinates:origin andDestinationCoordinate:destination];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate)
    {
        Airports *airport = [self.airportsModel.airports objectAtIndex:indexPath.row];
        [self.delegate setAirport:airport.name andCode:airport.code];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

@end
