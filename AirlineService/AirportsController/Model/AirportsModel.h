//
//  AirportsModel.h
//  AirlineService
//
//  Created by Андрей on 20.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface AirportsModel : NSObject

@property (nonatomic,strong) NSArray *airports;

- (void)getAirportsByCityCode:(NSString *)cityCode;
- (void)getNearAirportsByCoordinateWithLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude withComplition:(void (^)())completion;
- (NSString *)getCityCodeByCityName:(NSString *)cityName;
- (void)getCoordinateByCityName:(NSString *)cityName;

- (NSString *) getDistanceWithOriginCoordinates:(CLLocationCoordinate2D)origin andDestinationCoordinate:(CLLocationCoordinate2D)destination;

@end
