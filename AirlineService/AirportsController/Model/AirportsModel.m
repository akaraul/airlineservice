//
//  AirportsModel.m
//  AirlineService
//
//  Created by Андрей on 20.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "AirportsModel.h"
#import "CoreData/CoreData.h"
#import "Cities+CoreDataClass.h"
#import "FlightStatService.h"

#import "AppDelegate.h"

@interface AirportsModel ()

@property (nonatomic,strong) NSPersistentContainer *persistentContainer;

@end

@implementation AirportsModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}

- (void)getAirportsByCityCode:(NSString *)cityCode {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Airports"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityCode == %@",cityCode];
    request.predicate = predicate;
    
    self.airports = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
}

- (NSString *)getCityCodeByCityName:(NSString *)cityName {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cities"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@",cityName];
    request.predicate = predicate;
    [request setFetchLimit:1];
    
    NSArray *result = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    Cities *cityObj = nil;
    if ([result count] > 0) cityObj = [result firstObject];
    return cityObj.code;
}

- (void)getNearAirportsByCoordinateWithLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude withComplition:(void (^)())completion {
    [[FlightStatService sharedService] getAirportWithinRadius:150 withLatitude:latitude andLongitude:longitude withComplition:^(NSArray * result){
        self.airports = result;
        if (completion) {
            completion();
        }
    }];
}

- (void)getCoordinateByCityName:(NSString *)cityName {
    
}

- (NSString *) getDistanceWithOriginCoordinates:(CLLocationCoordinate2D)origin andDestinationCoordinate:(CLLocationCoordinate2D)destination {
    double longitudeOrigin = origin.longitude*M_PI/180;
    double latitudeOrigin = origin.latitude*M_PI/180;
    double longitudeDestination = destination.longitude*M_PI/180;
    double latitudeDestination = destination.latitude*M_PI/180;
    double distance = acos (sin(latitudeOrigin)*sin(latitudeDestination)+
                            cos(latitudeOrigin)*cos(latitudeDestination)*cos(longitudeOrigin-longitudeDestination))*6371;
    
    return [NSString stringWithFormat:@"∽%ldкм",lroundf(distance)];
}


@end
