//
//  AirportsView.m
//  AirlineService
//
//  Created by Андрей on 20.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "AirportsView.h"

@implementation AirportsView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _airportsTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self addSubview:_airportsTable];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    _indicatorView.color = [UIColor blueColor];
    [self addSubview:_indicatorView];
    
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    self.indicatorView.center = self.center;
    self.airportsTable.frame = self.bounds;
}


@end
