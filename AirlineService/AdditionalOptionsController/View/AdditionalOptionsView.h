//
//  AdditionalOptionsView.h
//  AirlineService
//
//  Created by Андрей on 21.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdditionalOptionsView : UIView

@property (nonatomic,strong) UILabel *maxPriceLabel;
@property (nonatomic,strong) UITextField *maxPriceTextField;

@property (nonatomic,strong) UILabel *earlistTimeLabel;
@property (nonatomic,strong) UITextField *earlistTime;
@property (nonatomic,strong) UIDatePicker *earlistTimePicker;
@property (nonatomic,strong) UILabel *latestTimeLabel;
@property (nonatomic,strong) UITextField *latestTime;
@property (nonatomic,strong) UIDatePicker *latestTimePicker;

@property (nonatomic,strong) UILabel *preferredCabinLabel;
@property (nonatomic,strong) UITextField *preferredCabin;
@property (nonatomic,strong) UIPickerView *preferredCabinValue;

@property (nonatomic,strong) UILabel *refundableLabel;
@property (nonatomic,strong) UISwitch *refundable;

@property (nonatomic,strong) UILabel *countChildLabel;
@property (nonatomic,strong) UILabel *countChild;
@property (nonatomic,strong) UIStepper *countChildStepper;

@property (nonatomic, assign) CGFloat topOffset;

@end
