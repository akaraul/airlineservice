//
//  AdditionalOptionsView.m
//  AirlineService
//
//  Created by Андрей on 21.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "AdditionalOptionsView.h"

@implementation AdditionalOptionsView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _latestTimePicker = [[UIDatePicker alloc] init];
    _latestTimePicker.datePickerMode = UIDatePickerModeTime;
    _earlistTimePicker = [[UIDatePicker alloc] init];
    _earlistTimePicker.datePickerMode = UIDatePickerModeTime;
    
    _maxPriceLabel = [[UILabel alloc]init];
    _maxPriceLabel.text = @"Максимальная цена";
    [self addSubview:_maxPriceLabel];
    
    _maxPriceTextField = [[UITextField alloc]init];
    _maxPriceTextField.borderStyle = UITextBorderStyleRoundedRect;
    _maxPriceTextField.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_maxPriceTextField];
    
    _earlistTimeLabel = [[UILabel alloc]init];
    _earlistTimeLabel.text = @"Мin время вылета";
    [self addSubview:_earlistTimeLabel];
    
    _earlistTime = [[UITextField alloc]init];
    _earlistTime.borderStyle = UITextBorderStyleRoundedRect;
    _earlistTime.placeholder = @"Выберите время";
    _earlistTime.textAlignment = NSTextAlignmentCenter;
    _earlistTime.inputView = self.earlistTimePicker;
    [self addSubview:_earlistTime];
    
    _latestTimeLabel = [[UILabel alloc]init];
    _latestTimeLabel.text = @"Маx время вылета";
    [self addSubview:_latestTimeLabel];
    
    _latestTime = [[UITextField alloc]init];
    _latestTime.borderStyle = UITextBorderStyleRoundedRect;
    _latestTime.placeholder = @"Выберите время";
    _latestTime.textAlignment = NSTextAlignmentCenter;
    _latestTime.inputView = self.latestTimePicker;
    [self addSubview:_latestTime];
    
    _preferredCabinLabel = [[UILabel alloc]init];
    _preferredCabinLabel.text = @"Класс обслуживания";
    [self addSubview:_preferredCabinLabel];
    
    _preferredCabinValue = [[UIPickerView alloc] init];
    
    _preferredCabin = [[UITextField alloc]init];
    _preferredCabin.inputView = self.preferredCabinValue;
    _preferredCabin.textAlignment = NSTextAlignmentCenter;
    _preferredCabin.borderStyle = UITextBorderStyleRoundedRect;
    [self addSubview:_preferredCabin];
    
    _refundableLabel = [[UILabel alloc]init];
    _refundableLabel.text = @"Возможен возврат";
    [self addSubview:_refundableLabel];
    
    _refundable = [[UISwitch alloc]init];
    [self addSubview:_refundable];
    
    _countChild = [[UILabel alloc]init];
    _countChild.text = @"0";
    [self addSubview:_countChild];
    
    _countChildLabel = [[UILabel alloc]init];
    _countChildLabel.text = @"Количество детей:";
    [self addSubview:_countChildLabel];
    
    _countChildStepper = [[UIStepper alloc]init];
    _countChildStepper.minimumValue = 0;
    _countChildStepper.maximumValue = 9;
    _countChildStepper.value = 0;
    [self addSubview:_countChildStepper];
    
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat heightInputField = 40.0f;
    CGFloat width = self.bounds.size.width;
    CGFloat labelWidth = width*0.55f;

    
    self.maxPriceLabel.frame = CGRectMake(offset,
                                          offset + self.topOffset,
                                          labelWidth,
                                          heightInputField);
    
    self.maxPriceTextField.frame = CGRectMake(self.maxPriceLabel.frame.origin.x + self.maxPriceLabel.bounds.size.width + offset,
                                    self.maxPriceLabel.frame.origin.y,
                                    width - offset*2 - self.maxPriceLabel.frame.origin.x - self.maxPriceLabel.bounds.size.width ,
                                    heightInputField);
    
    self.earlistTimeLabel.frame = CGRectMake(offset, self.maxPriceLabel.frame.origin.y + self.maxPriceLabel.bounds.size.height + offset, labelWidth, heightInputField);
    
    self.earlistTime.frame = CGRectMake(self.earlistTimeLabel.frame.origin.x + self.earlistTimeLabel.bounds.size.width + offset, self.earlistTimeLabel.frame.origin.y, width - offset*2 - self.earlistTimeLabel.frame.origin.x - self.earlistTimeLabel.bounds.size.width, heightInputField);
    
    self.latestTimeLabel.frame = CGRectMake(offset, self.earlistTimeLabel.frame.origin.y + self.earlistTimeLabel.bounds.size.height + offset, labelWidth, heightInputField);
    
    self.latestTime.frame = CGRectMake(self.latestTimeLabel.frame.origin.x + self.latestTimeLabel.bounds.size.width + offset, self.latestTimeLabel.frame.origin.y, width - offset*2 - self.latestTimeLabel.frame.origin.x - self.latestTimeLabel.bounds.size.width, heightInputField);
    
    self.preferredCabinLabel.frame = CGRectMake(offset, self.latestTimeLabel.frame.origin.y + self.latestTimeLabel.bounds.size.height + offset, labelWidth, heightInputField);
    
    self.preferredCabin.frame = CGRectMake(self.preferredCabinLabel.frame.origin.x + self.preferredCabinLabel.bounds.size.width + offset, self.preferredCabinLabel.frame.origin.y, width - offset*2 - self.preferredCabinLabel.frame.origin.x - self.preferredCabinLabel.bounds.size.width, heightInputField);
    
    self.refundableLabel.frame = CGRectMake(offset, self.preferredCabinLabel.frame.origin.y + self.preferredCabinLabel.bounds.size.height + offset, labelWidth, heightInputField);
    
    self.refundable.frame = CGRectMake(width - offset - self.refundable.bounds.size.width, self.refundableLabel.frame.origin.y, self.refundable.bounds.size.width, heightInputField);
    
    self.countChildLabel.frame = CGRectMake(offset, self.refundableLabel.frame.origin.y + self.refundableLabel.bounds.size.height + offset, labelWidth-25.0f, heightInputField);
    
    self.countChild.frame = CGRectMake(self.countChildLabel.frame.origin.x + self.countChildLabel.bounds.size.width + offset,
                                       self.countChildLabel.frame.origin.y,
                                       30.0f,
                                       heightInputField);
    
    self.countChildStepper.frame = CGRectMake(width - offset - self.countChildStepper.bounds.size.width,
                                              self.countChildLabel.frame.origin.y,
                                              self.countChildStepper.bounds.size.width,
                                              heightInputField);
    
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
