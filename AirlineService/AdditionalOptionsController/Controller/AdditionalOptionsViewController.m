//
//  AdditionalOptionsViewController.m
//  AirlineService
//
//  Created by Андрей on 21.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "AdditionalOptionsViewController.h"
#import "AdditionalOptionsView.h"

@interface AdditionalOptionsViewController () <UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic,strong) AdditionalOptionsView *optionsView;
@property (nonatomic,strong) NSArray *preferredCabin;
@property (nonatomic,strong) NSDictionary *preferredCabinValue;

@end

@implementation AdditionalOptionsViewController

- (void)loadView {
    self.optionsView = [[AdditionalOptionsView alloc] init];
    self.view = self.optionsView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.optionsView.preferredCabinValue.delegate = self;
    self.optionsView.preferredCabinValue.dataSource = self;
    
    self.preferredCabin = [NSArray arrayWithObjects:@"Coach",@"Premium coach",@"Business",@"First class",nil];
    
    self.preferredCabinValue = @{
                                 @"Coach" :             @"COACH",
                                 @"Premium coach" :   @"PREMIUM_COACH",
                                 @"Business" :           @"BUSINESS",
                                 @"First class" :        @"FIRST",
                                 };
    
    self.navigationItem.title = @"Дополнительные параметры";
    
    [self.optionsView.refundable addTarget:self action:@selector(switchEditing:) forControlEvents:UIControlEventValueChanged];
    [self.optionsView.maxPriceTextField addTarget:self action:@selector(setMaxPrice:) forControlEvents:UIControlEventEditingDidEnd];
    [self.optionsView.earlistTimePicker addTarget:self action:@selector(setEarlyTime:) forControlEvents:UIControlEventValueChanged];
    [self.optionsView.latestTimePicker addTarget:self action:@selector(setLatestTime:) forControlEvents:UIControlEventValueChanged];
    [self.optionsView.countChildStepper addTarget:self action:@selector(stepperChange:) forControlEvents:UIControlEventValueChanged];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.preferredCabin count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.preferredCabin objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.optionsView.preferredCabin.text = [self.preferredCabin objectAtIndex:row];
    NSString *value = [self.preferredCabinValue objectForKey:[self.preferredCabin objectAtIndex:row]];
    if (self.delegate) {
        [self.delegate updateOptionsWithValue:value forKeyPath:@"request.slice.preferredCabin"];
    }
}

- (void)setMaxPrice:(UITextField *)sender {
    NSString *value = [NSString stringWithFormat:@"RUB%@", sender.text];
    if (self.delegate) {
        [self.delegate updateOptionsWithValue:value forKeyPath:@"request.maxPrice"];
    }
}

- (void)switchEditing:(UISwitch *)sender {
    if (sender.on) {
        if (self.delegate) {
            [self.delegate updateOptionsWithValue:@"true" forKeyPath:@"request.refundable"];
        }
    }
    else {
        if (self.delegate) {
            [self.delegate updateOptionsWithValue:@"false" forKeyPath:@"request.refundable"];
        }
    }
}

- (void)setEarlyTime:(UIDatePicker *)sender {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *value = [dateFormat stringFromDate:sender.date];
    self.optionsView.earlistTime.text = value;
    if (self.delegate) {
        [self.delegate updateOptionsWithValue:value forKeyPath:@"request.slice.permittedDepartureTime.earliestTime"];
    }

}

- (void)setLatestTime:(UIDatePicker *)sender {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *value = [dateFormat stringFromDate:sender.date];
    self.optionsView.latestTime.text = value;
    if (self.delegate) {
        [self.delegate updateOptionsWithValue:value forKeyPath:@"request.slice.permittedDepartureTime.latestTime"];
    }
}

- (void)stepperChange:(UIStepper *)sender {
    self.optionsView.countChild.text = [NSString stringWithFormat:@"%ld",(long)sender.value];
    if (self.delegate) {
        [self.delegate updateOptionsWithValue:self.optionsView.countChild.text forKeyPath:@"request.passengers.childCount"];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.optionsView.topOffset = self.topLayoutGuide.length;
}

@end
