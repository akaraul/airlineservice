//
//  AdditionalOptionsViewController.h
//  AirlineService
//
//  Created by Андрей on 21.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  AdditionalOptionsViewControllerDelegate;

@interface AdditionalOptionsViewController : UIViewController

@property (nonatomic, weak) id<AdditionalOptionsViewControllerDelegate> delegate;

@end

@protocol AdditionalOptionsViewControllerDelegate <NSObject>

- (void)updateOptionsWithValue:(NSString *)value forKeyPath:(NSString *)keyPath;

@end
