//
//  DetailView.h
//  AirlineService
//
//  Created by Андрей on 21.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface DetailView : UIScrollView

@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic ,strong) UILabel *allTravel;

@property (nonatomic,strong) UITableView *travelTable;

@property (nonatomic, assign) CGFloat topOffset;

@end
