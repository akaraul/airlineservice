//
//  DetailView.m
//  AirlineService
//
//  Created by Андрей on 21.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "DetailView.h"

@implementation DetailView
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _mapView = [[MKMapView alloc] init];
    [self addSubview:_mapView];
    
    _allTravel = [[UILabel alloc] init];
    _allTravel.text = @"Весь маршрут:";
    [self addSubview:_allTravel];
    
    _travelTable = [[UITableView alloc] init];
    _travelTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_travelTable];
    
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    
    self.contentSize = CGSizeMake(self.bounds.size.width, self.bounds.size.height);
    
    self.mapView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height/2.0f);
    
    self.allTravel.frame = CGRectMake(offset, self.mapView.frame.origin.y + self.mapView.bounds.size.height + offset, self.bounds.size.width, 30.0f);
    
    self.travelTable.frame = CGRectMake(0, self.allTravel.frame.origin.y + self.allTravel.bounds.size.height + offset, self.bounds.size.width, self.bounds.size.height/4.0f);
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
