//
//  DetailViewModel.m
//  AirlineService
//
//  Created by Андрей on 23.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "DetailViewModel.h"
#import "CoreData/CoreData.h"
#import "Airports+CoreDataClass.h"

#import "AppDelegate.h"

@interface DetailViewModel ()

@property (nonatomic,strong) NSPersistentContainer *persistentContainer;

@end

@implementation DetailViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}


- (CLLocationCoordinate2D)getLocationAirportByCode:(NSString *)code {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Airports"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %@",code];
    request.predicate = predicate;
    [request setFetchLimit:1];
    
    NSArray *result = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    Airports *resObj = nil;
    if ([result count] > 0) resObj = [result firstObject];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(resObj.latitude,resObj.longitude);
    return coordinate;
}


@end
