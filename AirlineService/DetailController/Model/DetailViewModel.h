//
//  DetailViewModel.h
//  AirlineService
//
//  Created by Андрей on 23.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface DetailViewModel : NSObject

- (CLLocationCoordinate2D)getLocationAirportByCode:(NSString *)code;

@end
