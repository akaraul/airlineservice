//
//  DetailViewController.h
//  AirlineService
//
//  Created by Андрей on 21.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (nonatomic,strong) id resultItem;

@end
