//
//  DetailViewController.m
//  AirlineService
//
//  Created by Андрей on 21.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "DetailViewController.h"
#import "DetailView.h"
#import "MapKit/Mapkit.h"
#import "DetailViewModel.h"

static NSString *const ViewControllerMapViewPin = @"ViewControllerMapViewPin";

@interface DetailViewController () <MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic,strong) DetailView *detailView;
@property (nonatomic,strong) DetailViewModel *detailViewModel;
@property (nonatomic, strong) MKPolyline *routeLine;

@end

@implementation DetailViewController

- (void)loadView {
    self.detailView = [[DetailView alloc] init];
    self.view = self.detailView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Детали";
    
    self.detailView.mapView.delegate = self;
    self.detailView.travelTable.delegate = self;
    self.detailView.travelTable.dataSource = self;
    
    self.detailViewModel = [[DetailViewModel alloc] init];
    [self createRoute];
}

- (void)createRoute {
    NSArray *segment = [self.resultItem objectForKey:@"segment"];
    CLLocationCoordinate2D coordinateArray[2];
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    MKPolyline *routeLine;
    for (NSInteger i = 0; i<[segment count];i++) {
       coordinateArray[0] = [self.detailViewModel getLocationAirportByCode:[segment[i] objectForKey:@"origin"]];
       coordinateArray[1] = [self.detailViewModel getLocationAirportByCode:[segment[i] objectForKey:@"destination"]];
       routeLine = [MKPolyline polylineWithCoordinates:coordinateArray count:2];
       [self.detailView.mapView addOverlay:routeLine];
        MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
        [pointAnnotation setCoordinate:coordinateArray[0]];
        [annotations addObject:pointAnnotation];
    }
    MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
    [pointAnnotation setCoordinate:coordinateArray[1]];
    [annotations addObject:pointAnnotation];
    [self.detailView.mapView addAnnotations:annotations];
    [self.detailView.mapView showAnnotations:annotations animated:YES];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay {
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *render = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
        render.strokeColor = [UIColor redColor];
        render.lineWidth = 3;
        return render;
    }
    return nil;
}

- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    MKAnnotationView *pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:ViewControllerMapViewPin];
    if (!pinView) {
        pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                               reuseIdentifier:ViewControllerMapViewPin];
    }
    
    pinView.image = [UIImage imageNamed:@"airplane_black"];
    return pinView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *segment = [self.resultItem objectForKey:@"segment"];
    return [segment count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MYCell"];
    }
    cell.backgroundColor = [UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1];
    NSArray *segment = [self.resultItem objectForKey:@"segment"];
    NSString *res = [NSString stringWithFormat:@"%@ - %@",[[segment objectAtIndex:indexPath.row] objectForKey:@"origin"], [[segment objectAtIndex:indexPath.row] objectForKey:@"destination"]];
    cell.textLabel.text = res;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}


- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.detailView.topOffset = self.topLayoutGuide.length;
}

@end
