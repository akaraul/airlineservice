//
//  Mapper.h
//  AirlineService
//
//  Created by Андрей on 20.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreData/CoreData.h"

@interface Mapper : NSObject

+ (void)mapCityInContext:(NSManagedObjectContext *)context;
+ (void)mapCountryInContext:(NSManagedObjectContext *)context;
+ (void)mapAirportInContext:(NSManagedObjectContext *)context;

+ (NSMutableDictionary *)createJSONwithOptions;
+ (NSArray *)mapAirportsToArrayFromData:(NSData *)data;
+ (NSArray *)mapResponseJSONToArray:(NSData *)responseJSON;

@end
