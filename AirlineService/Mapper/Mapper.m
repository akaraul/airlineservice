//
//  Mapper.m
//  AirlineService
//
//  Created by Андрей on 20.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "Mapper.h"
#import "AppDelegate.h"

@implementation Mapper

+ (void)mapCityInContext:(NSManagedObjectContext *)context {
    NSString *path= [[NSBundle mainBundle] pathForResource:@"city" ofType:@"json"];
    NSData *output = [NSData dataWithContentsOfFile:path];
    NSArray *cities =[NSJSONSerialization JSONObjectWithData:output options:kNilOptions error:nil];
    
    for (id city in cities)
    {
        NSManagedObject *entity = [NSEntityDescription insertNewObjectForEntityForName:@"Cities"
                                                                inManagedObjectContext:context];
        [entity setValue:[city objectForKey:@"code"] forKey:@"code"];
        [entity setValue:[city objectForKey:@"name"] forKey:@"name"];
        [entity setValue:[city objectForKey:@"country_code"] forKey:@"countryCode"];
    }

    
}

+ (void)mapCountryInContext:(NSManagedObjectContext *)context {
    NSString *path= [[NSBundle mainBundle] pathForResource:@"country" ofType:@"json"];
    NSData *output = [NSData dataWithContentsOfFile:path];
    NSArray *countries =[NSJSONSerialization JSONObjectWithData:output options:kNilOptions error:nil];
    for (id country in countries)
    {
        NSManagedObject *entity = [NSEntityDescription insertNewObjectForEntityForName:@"Countries"
                                                                inManagedObjectContext:context];
        [entity setValue:[country objectForKey:@"code"] forKey:@"code"];
        [entity setValue:[country objectForKey:@"name"] forKey:@"name"];
    }

    
}

+ (void)mapAirportInContext:(NSManagedObjectContext *)context {
    NSString *path= [[NSBundle mainBundle] pathForResource:@"airport" ofType:@"json"];
    NSData *output = [NSData dataWithContentsOfFile:path];
    NSArray *airports =[NSJSONSerialization JSONObjectWithData:output options:kNilOptions error:nil];
    
    for (id country in airports)
    {
        NSManagedObject *entity = [NSEntityDescription insertNewObjectForEntityForName:@"Airports"
                                                                inManagedObjectContext:context];
        [entity setValue:[country objectForKey:@"iata"] forKey:@"code"];
        [entity setValue:[country objectForKey:@"name"] forKey:@"name"];
        [entity setValue:[country objectForKey:@"cityCode"] forKey:@"cityCode"];
        [entity setValue:[country objectForKey:@"latitude"] forKey:@"latitude"];
        [entity setValue:[country objectForKey:@"longitude"] forKey:@"longitude"];
    }
}

+ (NSArray *)mapAirportsToArrayFromData:(NSData *)data {
    NSMutableArray *result = [[NSMutableArray alloc]init];
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Airports"];
    NSPersistentContainer *container = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    [request setFetchLimit:1];

    NSArray *airports = [json valueForKeyPath:@"airports"];
    for (id currAirport in airports)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %@",[currAirport objectForKey:@"iata"]];
        request.predicate = predicate;
        NSArray *airportsArray = [container.viewContext executeFetchRequest:request error:nil];
        if ([airportsArray count] > 0)
        {
            [result addObject:[airportsArray firstObject]];
        }
    }
    return [NSArray arrayWithArray:result];
}

+ (NSMutableDictionary *)createJSONwithOptions {
    
    NSMutableDictionary *passengers = [[NSMutableDictionary alloc]init];
    [passengers setObject:@"1" forKey:@"adultCount"];
    [passengers setObject:@"0" forKey:@"childCount"];
    [passengers setObject:@"0" forKey:@"infantInLapCount"];
    [passengers setObject:@"0" forKey:@"infantInSeatCount"];
    [passengers setObject:@"0" forKey:@"seniorCount"];
    
    NSMutableDictionary *permittedDepartureTime =[[NSMutableDictionary alloc]init];
    [permittedDepartureTime setObject:[NSNull null] forKey:@"earliestTime"];
    [permittedDepartureTime setObject:[NSNull null] forKey:@"latestTime"];
    
    NSMutableDictionary *sliceInput = [[NSMutableDictionary alloc] init];
    [sliceInput setObject:[NSNull null] forKey:@"origin"];
    [sliceInput setObject:[NSNull null] forKey:@"destination"];
    [sliceInput setObject:[NSNull null] forKey:@"date"];
    [sliceInput setObject:[NSNull null] forKey:@"maxStops"];
    [sliceInput setObject:[NSNull null] forKey:@"preferredCabin"];
    [sliceInput setObject:permittedDepartureTime forKey:@"permittedDepartureTime"];
    
    NSMutableArray *slice = [[NSMutableArray alloc]init];
    [slice addObject:sliceInput];
    
    NSMutableDictionary *request = [[NSMutableDictionary alloc]init];
    [request setObject:slice forKey:@"slice"];
    [request setObject:passengers forKey:@"passengers"];
    [request setObject:[NSNull null] forKey:@"maxPrice"];
    [request setObject:[NSNull null] forKey:@"refundable"];
    [request setObject:@"20" forKey:@"solutions"];
    
    NSMutableDictionary *requestJSON = [NSMutableDictionary dictionaryWithObjectsAndKeys:request,@"request", nil];\
    return requestJSON;
}

+ (NSArray *)mapResponseJSONToArray:(NSData *)responseJSON {
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseJSON
                                                         options:0
                                                           error:nil];
    
    NSArray *tripOptions= [[json objectForKey:@"trips"] objectForKey:@"tripOption"];
    NSMutableArray *trips = [[NSMutableArray alloc] init];
    for (id tripOption in tripOptions)
    {
        NSMutableArray *_segmnet = [[NSMutableArray alloc] init];
        NSMutableDictionary *trip = [[NSMutableDictionary alloc] init];
        [trip setObject:[tripOption valueForKey:@"saleTotal"] forKey:@"saleTotal"];
        NSArray *slices= [tripOption objectForKey:@"slice"];
        for (id slice in slices){
            NSArray *segments = [slice objectForKey:@"segment"];
            for (id segment in segments){
                NSArray *legs = [segment objectForKey:@"leg"];
                for (id leg in legs){
                    NSMutableDictionary *_leg = [[NSMutableDictionary alloc] init];
                    [_leg setObject:[leg valueForKey:@"arrivalTime"] forKey:@"arrivalTime"];
                    [_leg setObject:[leg valueForKey:@"departureTime"] forKey:@"departureTime"];
                    [_leg setObject:[leg valueForKey:@"origin"] forKey:@"origin"];
                    [_leg setObject:[leg valueForKey:@"destination"] forKey:@"destination"];
                    [_segmnet addObject:_leg];
                }
            }
        }
        [trip setObject:_segmnet forKey:@"segment"];
        [trips addObject:trip];
    }
    NSArray *solutions = trips;
    return solutions;
}


@end
