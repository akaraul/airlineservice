//
//  ResultView.m
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "ResultView.h"

@implementation ResultView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    _resultTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _resultTable.backgroundColor = [UIColor colorWithRed:222/255.0 green:227/255.0 blue:234/255.0 alpha:1];
    _resultTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_resultTable];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.resultTable.frame = self.bounds;
    
}

@end
