//
//  ResultTableViewCell.h
//  AirlineService
//
//  Created by Андрей on 20.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultViewCellContent.h"

@interface ResultTableViewCell : UITableViewCell
@property (nonatomic, strong) ResultViewCellContent *cellContentView;

+ (NSString *)reuseIdentifier;

- (instancetype)init;
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;

- (void)setItem:(id)item;

@end
