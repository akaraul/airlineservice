//
//  ResultViewCellContent.m
//  AirlineService
//
//  Created by Андрей on 20.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "ResultViewCellContent.h"

@implementation ResultViewCellContent

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.textAlignment = NSTextAlignmentCenter;
        _priceLabel.textColor = [UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1];
        _priceLabel.font = [UIFont systemFontOfSize:22];
        [self addSubview:_priceLabel];
        
        _timeFromLabel = [[UILabel alloc] init];
        _timeFromLabel.textColor =[UIColor lightGrayColor];
        _timeFromLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_timeFromLabel];
        
        _timeToLabel = [[UILabel alloc] init];
        _timeToLabel.textColor =[UIColor lightGrayColor];
        _timeToLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_timeToLabel];
        
        _countStops = [[UILabel alloc] init];
        _countStops.text = @"-";
        _countStops.textAlignment = NSTextAlignmentCenter;
        _countStops.font = [UIFont systemFontOfSize:16];
        [self addSubview:_countStops];
        
        _stopsLabel = [[UILabel alloc] init];
        _stopsLabel.text = @"Пересадки";
        _stopsLabel.textColor =[UIColor lightGrayColor];
        _stopsLabel.textAlignment = NSTextAlignmentCenter;
        _stopsLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_stopsLabel];
        
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.font = [UIFont systemFontOfSize:16];
        [self addSubview:_nameLabel];
        
        _addToLiked = [[UIButton alloc] init];
        [_addToLiked setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
        [self addSubview:_addToLiked];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat width = self.bounds.size.width;
    CGFloat widthPriceLabel = width*0.4f;
    CGFloat widthLabel = width*0.33f;
    
    self.priceLabel.frame = CGRectMake(offset, offset, widthPriceLabel, 30.0f);
    
    self.nameLabel.frame = CGRectMake(self.priceLabel.frame.origin.x + self.priceLabel.bounds.size.width + offset,
                                      self.priceLabel.frame.origin.y,
                                      widthLabel,
                                      30.0f);
    
    self.timeFromLabel.frame = CGRectMake(offset,
                                      self.priceLabel.frame.origin.y + self.priceLabel.bounds.size.height + offset,
                                      width*0.63f,
                                      30.0f);
    self.timeToLabel.frame = CGRectMake(offset,
                                        self.timeFromLabel.frame.origin.y + self.timeFromLabel.bounds.size.height, width*0.64f,
                                        30.0f);
    
    self.stopsLabel.frame = CGRectMake(self.timeFromLabel.frame.origin.x+self.timeFromLabel.bounds.size.width + offset,
                                       self.timeFromLabel.frame.origin.y,
                                       width*0.3f,
                                       30.0f);
    
    self.countStops.frame = CGRectMake(self.stopsLabel.frame.origin.x,
                                       self.stopsLabel.frame.origin.y + self.stopsLabel.bounds.size.height,
                                       width*0.3f,
                                       20.0f);
    
    self.addToLiked.frame = CGRectMake(width - 40.0f,
                                       0,
                                       40.0f,
                                       40.0f);
}

- (void)setItem:(id)item {
    NSArray *segment = [item objectForKey:@"segment"];
    self.priceLabel.text = [item objectForKey:@"saleTotal"];
    self.nameLabel.text = [NSString stringWithFormat:@"%@ - %@",[[segment firstObject] objectForKey:@"origin"],[[segment lastObject] objectForKey:@"destination"]];
    if ([segment count] > 1) {
        self.countStops.text = [NSString stringWithFormat:@"%lu",(unsigned long)[segment count]-1];
    }
    NSArray *timeDep = [[[segment firstObject] objectForKey:@"departureTime"] componentsSeparatedByString:@"T"];
    self.timeFromLabel.text = [NSString stringWithFormat:@"Вылет:%@ %@",
                               timeDep[0],
                               [timeDep[1] componentsSeparatedByString:@"+"][0]];
    NSArray *timeArr = [[[segment lastObject] objectForKey:@"arrivalTime"] componentsSeparatedByString:@"T"];
    self.timeToLabel.text = [NSString stringWithFormat:@"Прибытие:%@ %@",
                             timeArr[0],
                             [timeArr[1] componentsSeparatedByString:@"+"][0]];
    
    [self setNeedsLayout];
}

- (void)prepareForReuse {
    self.countStops.text = @"-";
}


@end
