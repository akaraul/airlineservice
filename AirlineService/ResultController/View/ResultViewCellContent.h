//
//  ResultViewCellContent.h
//  AirlineService
//
//  Created by Андрей on 20.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewCellContent : UIView

@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *timeFromLabel;
@property (nonatomic, strong) UILabel *timeToLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *stopsLabel;
@property (nonatomic, strong) UILabel *countStops;

@property (nonatomic, strong) UIButton *addToLiked;

- (void)setItem:(id)item;

- (void)prepareForReuse;

@end
