//
//  ResultViewModel.m
//  AirlineService
//
//  Created by Андрей on 23.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "ResultViewModel.h"
#import "CoreData/CoreData.h"
#import "Favorite+CoreDataClass.h"
#import "PairAirports+CoreDataClass.h"
#import "AppDelegate.h"

@interface ResultViewModel ()

@property (nonatomic,strong) NSPersistentContainer *persistentContainer;

@end

@implementation ResultViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}

- (void)addLikedToDataBase:(id)object withComplition:(void (^)())completion {
    
    Favorite *favorite = [NSEntityDescription insertNewObjectForEntityForName:@"Favorite" inManagedObjectContext:self.persistentContainer.viewContext];
    NSMutableOrderedSet *pairAirports = [[NSMutableOrderedSet alloc] init];
    NSArray *segment = [object objectForKey:@"segment"];
    for (id curr in segment) {
        PairAirports *pair = [NSEntityDescription insertNewObjectForEntityForName:@"PairAirports" inManagedObjectContext:self.persistentContainer.viewContext];
        pair.airportFrom = [curr objectForKey:@"origin"];
        pair.airportTo = [curr objectForKey:@"destination"];
        pair.timeArrival = [curr objectForKey:@"arrivalTime"];
        pair.timeDeparture = [curr objectForKey:@"departureTime"];
        [pairAirports addObject:pair];
    }
    favorite.saleTotal = [object objectForKey:@"saleTotal"];
    
    favorite.pairAirports = pairAirports;
    
    NSError *error = nil;
    [self.persistentContainer.viewContext save:&error];
    if (completion){
        completion();
    }
}

@end
