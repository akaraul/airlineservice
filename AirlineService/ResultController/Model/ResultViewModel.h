//
//  ResultViewModel.h
//  AirlineService
//
//  Created by Андрей on 23.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResultViewModel : NSObject

- (void)addLikedToDataBase:(id)object withComplition:(void (^)())completion;

@end
