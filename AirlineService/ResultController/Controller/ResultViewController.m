//
//  ResultViewController.m
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "ResultViewController.h"
#import "ResultView.h"
#import "ResultTableViewCell.h"
#import "DetailViewController.h"
#import "ResultViewModel.h"

@interface ResultViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) ResultView *resultView;
@property (nonatomic,strong) ResultViewModel *resultViewModel;

@end

@implementation ResultViewController

-(void)loadView{
    self.resultView = [[ResultView alloc] init];
    self.view = self.resultView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.resultViewModel = [[ResultViewModel alloc] init];
    
    self.navigationItem.title = @"Результаты поиска";
    
    self.resultView.resultTable.delegate = self;
    self.resultView.resultTable.dataSource = self;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.resultArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ResultTableViewCell reuseIdentifier]];
    if (!cell) {
        cell = [[ResultTableViewCell alloc] init];
    }
    [cell.cellContentView.addToLiked addTarget:self action:@selector(addToLiked:) forControlEvents:UIControlEventTouchUpInside];
    cell.cellContentView.addToLiked.tag = indexPath.row;
    [cell setItem:[self.resultArray objectAtIndex:indexPath.row]];
    return cell;
}

- (void)addToLiked:(UIButton *)sender {
    __weak typeof(self) weakSelf = self;
    [self.resultViewModel addLikedToDataBase:[self.resultArray objectAtIndex:sender.tag] withComplition:^{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Добавлено в избранное!"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"ОК"
                                    style:UIAlertActionStyleDefault
                                    handler:nil];
        
        [alert addAction:yesButton];
        [weakSelf presentViewController:alert animated:YES completion:nil];
    }];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DetailViewController *detailViewController = [[DetailViewController alloc]init];
    detailViewController.resultItem = [self.resultArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130.0f;
}

@end
