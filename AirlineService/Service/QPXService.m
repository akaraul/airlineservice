//
//  QPXService.m
//  AirlineService
//
//  Created by Андрей on 13.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "QPXService.h"
#import "Mapper.h"

static NSString const *qpxAPIKey = @"AIzaSyDbg8Rc_VlXAxXXORhUZdSLHaFW6NCS7ck";

@interface QPXService ()

@end

@implementation QPXService

+ (instancetype)sharedService {
    static QPXService *sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[QPXService alloc] init];
    });
    return sharedService;
}

- (void)getResult:(NSDictionary *)options withComplition:(void (^)(NSArray * result))completion {
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyD1UDJSju9F1ixT2IjJMizscdzcbWO3EXo"]];
    
    //AIzaSyAC6N7k6ngCQ_gVvh_f1eH8rxZy5lPkCsk
    //?key=AIzaSyDbg8Rc_VlXAxXXORhUZdSLHaFW6NCS7ck //mykey
    NSData *postData = [NSJSONSerialization dataWithJSONObject:options options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    [request setHTTPMethod:@"POST"];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (completion) {
            completion([Mapper mapResponseJSONToArray:data]);
        }
    }]resume];
}

@end
