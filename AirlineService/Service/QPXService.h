//
//  QPXService.h
//  AirlineService
//
//  Created by Андрей on 13.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QPXService : NSObject

+ (instancetype)sharedService;

- (void)getResult:(NSDictionary *)options withComplition:(void (^)(NSArray *))completion;

@end
