//
//  FlightStatService.m
//  AirlineService
//
//  Created by Андрей on 20.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "FlightStatService.h"
#import "Mapper.h"

@implementation FlightStatService

+ (instancetype)sharedService {
    static FlightStatService *sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[FlightStatService alloc] init];
    });
    return sharedService;
}

- (void)getAirportWithinRadius:(NSInteger)radius withLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude withComplition:(void (^)(NSArray * result))completion {
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.flightstats.com/flex/airports/rest/v1/json/withinRadius/%@/%@/%li?appId=18e37951&appKey=f4f92941056c6362f1ab679d1deebafe",
                                       longitude,
                                       latitude,
                                       (long)radius]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *sess = [NSURLSession sessionWithConfiguration:configuration];
    
    [[sess dataTaskWithURL:url
         completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
             NSArray * result = [Mapper mapAirportsToArrayFromData:data];
             if (completion) {
                 completion(result);
             }
         }] resume];

    
}

@end
