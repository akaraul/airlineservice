//
//  FlightStatService.h
//  AirlineService
//
//  Created by Андрей on 20.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlightStatService : NSObject

+ (instancetype)sharedService;

- (void)getAirportWithinRadius:(NSInteger)radius withLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude withComplition:(void (^)(NSArray * result))completion;

@end
