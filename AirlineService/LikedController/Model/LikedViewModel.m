//
//  LikedViewModel.m
//  AirlineService
//
//  Created by Андрей on 24.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "LikedViewModel.h"
#import "CoreData/CoreData.h"
#import "AppDelegate.h"
#import "PairAirports+CoreDataClass.h"

@interface LikedViewModel ()

@property (nonatomic,strong) NSPersistentContainer *persistentContainer;

@end

@implementation LikedViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}

- (void)deleteLikedItem:(Favorite *)item {
    NSArray *pairAirports = [item.pairAirports array];
    for (PairAirports *pair in pairAirports) {
        [self.persistentContainer.viewContext deleteObject:pair];
    }
    [self.persistentContainer.viewContext deleteObject:item];
    [self.persistentContainer.viewContext save:nil];
}

@end
