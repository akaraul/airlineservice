//
//  LikedViewModel.h
//  AirlineService
//
//  Created by Андрей on 24.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Favorite+CoreDataClass.h"

@interface LikedViewModel : NSObject

- (void)deleteLikedItem:(Favorite *)item;

@end
