//
//  TableViewCellContent.m
//  AirlineService
//
//  Created by Андрей on 23.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "TableViewCellContent.h"
#import "PairAirports+CoreDataClass.h"

@implementation TableViewCellContent

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.textAlignment = NSTextAlignmentCenter;
        _priceLabel.textColor = [UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1];
        _priceLabel.font = [UIFont systemFontOfSize:22];
        [self addSubview:_priceLabel];
        
        _timeFromLabel = [[UILabel alloc] init];
        _timeFromLabel.textColor =[UIColor lightGrayColor];
        _timeFromLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_timeFromLabel];
        
        _timeToLabel = [[UILabel alloc] init];
        _timeToLabel.textColor =[UIColor lightGrayColor];
        _timeToLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_timeToLabel];
        
        _countStops = [[UILabel alloc] init];
        _countStops.text = @"-";
        _countStops.textAlignment = NSTextAlignmentCenter;
        _countStops.font = [UIFont systemFontOfSize:16];
        [self addSubview:_countStops];
        
        _stopsLabel = [[UILabel alloc] init];
        _stopsLabel.text = @"Пересадки";
        _stopsLabel.textColor =[UIColor lightGrayColor];
        _stopsLabel.textAlignment = NSTextAlignmentCenter;
        _stopsLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_stopsLabel];
        
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.font = [UIFont systemFontOfSize:16];
        [self addSubview:_nameLabel];

    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat width = self.bounds.size.width;
    CGFloat widthPriceLabel = width*0.4f;
    CGFloat widthLabel = width*0.33f;
    
    self.priceLabel.frame = CGRectMake(offset, offset, widthPriceLabel, 30.0f);
    
    self.nameLabel.frame = CGRectMake(self.priceLabel.frame.origin.x + self.priceLabel.bounds.size.width + offset,
                                      self.priceLabel.frame.origin.y,
                                      widthLabel,
                                      30.0f);
    
    self.timeFromLabel.frame = CGRectMake(offset,
                                          self.priceLabel.frame.origin.y + self.priceLabel.bounds.size.height + offset,
                                          width*0.6f,
                                          30.0f);
    self.timeToLabel.frame = CGRectMake(offset,
                                        self.timeFromLabel.frame.origin.y + self.timeFromLabel.bounds.size.height, width*0.6f,
                                        30.0f);
    
    self.stopsLabel.frame = CGRectMake(self.timeFromLabel.frame.origin.x+self.timeFromLabel.bounds.size.width + offset,
                                       self.timeFromLabel.frame.origin.y,
                                       width*0.25f,
                                       30.0f);
    
    self.countStops.frame = CGRectMake(self.stopsLabel.frame.origin.x,
                                       self.stopsLabel.frame.origin.y + self.stopsLabel.bounds.size.height,
                                       width*0.25f,
                                       20.0f);
    
}

- (void)setItem:(Favorite *)item {
    self.priceLabel.text = item.saleTotal;
    self.countStops.text = [NSString stringWithFormat:@"%lu",[item.pairAirports count]-1];
    NSString *from = [NSString stringWithFormat:@"%@",[[item.pairAirports array] firstObject].airportFrom];
    NSString *to = [NSString stringWithFormat:@"%@",[[item.pairAirports array] lastObject].airportTo];
    self.nameLabel.text = [NSString stringWithFormat:@"%@ - %@",from,to];
    self.timeToLabel.text = [NSString stringWithFormat:@"Вылет:%@",[[item.pairAirports array] firstObject].timeArrival];
    self.timeFromLabel.text = [NSString stringWithFormat:@"Прибытие:%@",[[item.pairAirports array] lastObject].timeDeparture];
    [self setNeedsLayout];
}

- (void)prepareForReuse {

}


@end
