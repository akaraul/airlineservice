//
//  LikedView.m
//  AirlineService
//
//  Created by Андрей on 10.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "LikedView.h"

@implementation LikedView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor grayColor];
    
    _likedTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _likedTable.backgroundColor = [UIColor colorWithRed:222/255.0 green:227/255.0 blue:234/255.0 alpha:1];
    _likedTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_likedTable];
    

    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    self.likedTable.frame = self.bounds;
    
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
