//
//  LikedTableViewCell.h
//  AirlineService
//
//  Created by Андрей on 23.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellContent.h"
#import "Favorite+CoreDataClass.h"

@interface LikedTableViewCell : UITableViewCell

@property (nonatomic, strong) TableViewCellContent *cellContentView;

+ (NSString *)reuseIdentifier;

- (instancetype)init;
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;

- (void)setItem:(Favorite *)item;

@end
