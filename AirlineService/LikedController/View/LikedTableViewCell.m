//
//  LikedTableViewCell.m
//  AirlineService
//
//  Created by Андрей on 23.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "LikedTableViewCell.h"

static CGFloat const ViewContentMargin = 8.0f;

@interface LikedTableViewCell ()

@property (nonatomic, assign) UIEdgeInsets contentInsets;

@end

@implementation LikedTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}

- (instancetype)init {
    return [self initWithReuseIdentifier:[LikedTableViewCell reuseIdentifier]];
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.backgroundColor = [UIColor colorWithRed:222/255.0 green:227/255.0 blue:234/255.0 alpha:1];
        
        _contentInsets = UIEdgeInsetsMake(ViewContentMargin,
                                          ViewContentMargin,
                                          ViewContentMargin,
                                          ViewContentMargin);
        
        _cellContentView = [[TableViewCellContent alloc] init];
        _cellContentView.layer.cornerRadius = 10.0f;
        _cellContentView.backgroundColor = [UIColor whiteColor];
        
        [self.contentView addSubview:_cellContentView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.cellContentView.frame = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
}

- (void)setItem:(Favorite *)item {
    [self.cellContentView setItem:item];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.cellContentView prepareForReuse];
}


@end
