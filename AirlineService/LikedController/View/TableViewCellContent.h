//
//  TableViewCellContent.h
//  AirlineService
//
//  Created by Андрей on 23.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Favorite+CoreDataClass.h"

@interface TableViewCellContent : UIView

@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *timeFromLabel;
@property (nonatomic, strong) UILabel *timeToLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *stopsLabel;
@property (nonatomic, strong) UILabel *countStops;

- (void)setItem:(Favorite *)item;

- (void)prepareForReuse;

@end
