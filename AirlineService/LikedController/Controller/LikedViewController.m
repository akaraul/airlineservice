//
//  LikedViewController.m
//  AirlineService
//
//  Created by Андрей on 10.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "LikedViewController.h"
#import "LikedView.h"
#import "LikedTableViewCell.h"
#import "LikedViewModel.h"
#import "CoreData/CoreData.h"
#import "AppDelegate.h"

@interface LikedViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,strong) LikedView *likedView;
@property (nonatomic,strong) LikedViewModel *likedViewModel;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSPersistentContainer *persistentContainer;

@end

@implementation LikedViewController

-(void)loadView {
    self.likedView = [[LikedView alloc] init];
    _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    self.view = self.likedView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.likedViewModel = [[LikedViewModel alloc]init];
    
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Favorite"];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"origin" ascending:YES];
    [request setSortDescriptors:@[sort]];
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc]
                                     initWithFetchRequest:request
                                     managedObjectContext:context
                                     sectionNameKeyPath:nil
                                     cacheName:nil];
    [self.fetchedResultsController performFetch:nil];
    
    self.navigationItem.title = @"Избранное";
    self.likedView.likedTable.dataSource = self;
    self.likedView.likedTable.delegate = self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.fetchedResultsController.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LikedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[LikedTableViewCell reuseIdentifier]];
    if (!cell) {
        cell = [[LikedTableViewCell alloc] init];
    }
    [cell setItem:[self.fetchedResultsController objectAtIndexPath:indexPath]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130.0f;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
        return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.likedViewModel deleteLikedItem:[self.fetchedResultsController objectAtIndexPath:indexPath]];
    [self.fetchedResultsController performFetch:nil];
    [self.likedView.likedTable reloadData];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.likedView.topOffset = self.topLayoutGuide.length;
}


@end
