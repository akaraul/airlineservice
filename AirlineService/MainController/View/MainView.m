//
//  MainView.m
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "MainView.h"

@implementation MainView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _buttonSearch = [[UIButton alloc] init];
    [_buttonSearch setTitle:@"Поиск" forState:UIControlStateNormal];
    _buttonSearch.backgroundColor = [UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1];
    _buttonSearch.layer.cornerRadius = 10.0f;
    [self addSubview:_buttonSearch];
    
    _additionalOptions = [[UIButton alloc] init];
    [_additionalOptions setTitle:@"Дополнительные параметры" forState:UIControlStateNormal];
    _additionalOptions.backgroundColor = [UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1];
    _additionalOptions.layer.cornerRadius = 8.0f;
    [self addSubview:_additionalOptions];
    
    _labelFrom = [[UILabel alloc] init];
    _labelFrom.text = @"Откуда";
    [self addSubview:_labelFrom];
    
    _labelTo = [[UILabel alloc] init];
    _labelTo.text = @"Куда";
    [self addSubview:_labelTo];
    
    _labelDate = [[UILabel alloc] init];
    _labelDate.text = @"Дата вылета";
    [self addSubview:_labelDate];
    
    _labelNonStop = [[UILabel alloc] init];
    _labelNonStop.text = @"Без пересадок";
    [self addSubview:_labelNonStop];
    
    _textFieldFrom = [[UITextField alloc] init];
    _textFieldFrom.tag = 1;
    _textFieldFrom.placeholder = @"Выберите аэропорт";
    _textFieldFrom.textAlignment = NSTextAlignmentCenter;
    _textFieldFrom.borderStyle = UITextBorderStyleRoundedRect;
    [self addSubview:_textFieldFrom];
    
    _textFieldTo = [[UITextField alloc] init];
    _textFieldTo.placeholder = @"Выберите аэропорт";
    _textFieldTo.textAlignment = NSTextAlignmentCenter;
    _textFieldTo.borderStyle = UITextBorderStyleRoundedRect;
    [self addSubview:_textFieldTo];
    
    _datePicker = [[UIDatePicker alloc] init];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    _datePickerField = [[UITextField alloc] init];
    _datePickerField.textAlignment = NSTextAlignmentCenter;
    _datePickerField.placeholder = @"Выберите дату";
    _datePickerField.borderStyle = UITextBorderStyleRoundedRect;
    _datePickerField.inputView = self.datePicker;
    _datePickerField.text = [dateFormatter stringFromDate:[NSDate date]];
    [self addSubview:_datePickerField];
    
    _switchNonStop = [[UISwitch alloc] init];
    [self addSubview:_switchNonStop];
    
    _countAdultLabel = [[UILabel alloc]init];
    _countAdultLabel.text = @"Кол-во билетов:";
    [self addSubview:_countAdultLabel];
    
    _countAdult = [[UILabel alloc]init];
    _countAdult.text = @"1";
    [self addSubview:_countAdult];
    
    _countAdultStepper = [[UIStepper alloc]init];
    _countAdultStepper.minimumValue = 1;
    _countAdultStepper.maximumValue = 9;
    _countAdultStepper.value = 1;
    [self addSubview:_countAdultStepper];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    _indicatorView.color = [UIColor blueColor];
    [self addSubview:_indicatorView];
    
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat heightInputField = 40.0f;
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGFloat searchButtonWidth = width*0.6f;
    CGFloat searchButtonHeightFrame = height*0.85f;
    CGFloat optionButtonWidth = width * 0.8f;
    CGFloat labelWidth = 100.0f;
    
    self.indicatorView.center = self.center;
    
    self.labelFrom.frame = CGRectMake(offset,
                                      offset + self.topOffset,
                                      labelWidth,
                                      heightInputField);
    
    self.textFieldFrom.frame = CGRectMake(offset*2 + self.labelFrom.bounds.size.width,
                                          self.labelFrom.frame.origin.y,
                                          width - offset*2 - self.labelFrom.frame.origin.x - self.labelFrom.bounds.size.width ,
                                          heightInputField);
    
    self.labelTo.frame = CGRectMake(self.labelFrom.frame.origin.x,
                                    self.labelFrom.frame.origin.y + self.labelFrom.bounds.size.height + offset,
                                    labelWidth,
                                    heightInputField);
    
    self.textFieldTo.frame = CGRectMake(self.textFieldFrom.frame.origin.x,
                                        self.labelTo.frame.origin.y,
                                        self.textFieldFrom.bounds.size.width,
                                        heightInputField);
    
    self.labelDate.frame = CGRectMake(self.labelFrom.frame.origin.x,
                                      self.labelTo.frame.origin.y + self.labelTo.bounds.size.height + offset,
                                      labelWidth,
                                      heightInputField);
    
    self.datePickerField.frame = CGRectMake(self.textFieldFrom.frame.origin.x,
                                            self.labelDate.frame.origin.y,
                                            width - offset*2 - self.labelFrom.frame.origin.x - self.labelFrom.bounds.size.width ,
                                            heightInputField);
    
    self.labelNonStop.frame = CGRectMake(self.labelDate.frame.origin.x,
                                         self.labelDate.frame.origin.y + self.labelDate.bounds.size.height + offset, labelWidth+30,
                                         heightInputField);
    
    self.switchNonStop.frame = CGRectMake(width - offset - self.switchNonStop.bounds.size.width,
                                          self.labelNonStop.frame.origin.y,
                                          self.switchNonStop.bounds.size.width,
                                          heightInputField);
    
    self.countAdultLabel.frame = CGRectMake(self.labelNonStop.frame.origin.x,
                                            self.labelNonStop.frame.origin.y + self.labelNonStop.bounds.size.height + offset,
                                            labelWidth+30,
                                            heightInputField);
    
    self.countAdult.frame = CGRectMake(self.countAdultLabel.frame.origin.x + self.countAdultLabel.bounds.size.width + offset,
                                       self.countAdultLabel.frame.origin.y,
                                       30.0f,
                                       heightInputField);
    
    self.countAdultStepper.frame = CGRectMake(width - offset - self.countAdultStepper.bounds.size.width,
                                       self.countAdultLabel.frame.origin.y,
                                       self.countAdultStepper.bounds.size.width,
                                       heightInputField);
    
    self.additionalOptions.frame = CGRectMake((width - optionButtonWidth)/2.0f,
                                          self.countAdultLabel.frame.origin.y + self.countAdultLabel.bounds.size.height + offset,
                                          optionButtonWidth,
                                          30.0f);
    
    self.buttonSearch.frame = CGRectMake((width - searchButtonWidth)/2.0f,
                                         searchButtonHeightFrame,
                                         searchButtonWidth,
                                         50.0f);
    
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
