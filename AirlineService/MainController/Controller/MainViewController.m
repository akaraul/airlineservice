//
//  MainViewController.m
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "MainViewController.h"
#import "MainView.h"
#import "CountryViewController.h"
#import "ResultViewController.h"
#import "LikedViewController.h"
#import "AirportsViewController.h"
#import "QPXService.h"
#import "Mapper.h"
#import "AdditionalOptionsViewController.h"

@interface MainViewController () <UITextFieldDelegate, AirportsViewControllerDelegate, AdditionalOptionsViewControllerDelegate>

@property (nonatomic,strong) MainView *mainView;
@property (nonatomic, strong) UITextField *selectedTextField;
@property (nonatomic, strong) NSMutableDictionary *mainOptions;

@end

@implementation MainViewController

- (void)loadView {
    self.mainView = [[MainView alloc]init];
    self.view = self.mainView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainOptions = [NSMutableDictionary dictionaryWithDictionary:[Mapper createJSONwithOptions]];
    
    self.navigationItem.title = @"Поиск авиабилетов";
    
    self.mainView.textFieldTo.delegate = self;
    self.mainView.textFieldFrom.delegate = self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"star"] style:UIBarButtonItemStyleDone target:self action:@selector(getLiked:)];
    
    [self.mainView.switchNonStop addTarget:self action:@selector(switchEditing:) forControlEvents:UIControlEventValueChanged];
    [self.mainView.datePicker addTarget:self action:@selector(dateValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.mainView.buttonSearch addTarget:self action:@selector(getResult:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView.countAdultStepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.mainView.additionalOptions addTarget:self action:@selector(addOptions:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    [self.mainOptions setValue:[dateFormat stringFromDate:self.mainView.datePicker.date] forKeyPath:@"request.slice.date"];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.selectedTextField = textField;
    CountryViewController *countryController = [[CountryViewController alloc] init];
    countryController.typeOfController = CountryViewControllerTypeCountry;
    [self.navigationController pushViewController:countryController animated:YES];
    [self.view endEditing:YES];
    return NO;
}

- (void)stepperValueChanged:(UIStepper *)sender {
    self.mainView.countAdult.text = [NSString stringWithFormat:@"%ld",(long)sender.value];
    [self.mainOptions setValue:self.mainView.countAdult.text forKeyPath:@"request.passengers.adultCount"];
}

- (void)dateValueChanged:(UIDatePicker *)sender {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    self.mainView.datePickerField.text = [dateFormat stringFromDate:sender.date];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    [self.mainOptions setValue:[dateFormat stringFromDate:sender.date] forKeyPath:@"request.slice.date"];
}

- (void)switchEditing:(UISwitch *)sender {
    if (sender.on) {
        [self.mainOptions setValue:@"0" forKeyPath:@"request.slice.maxStops"];
    }
    else {
        [self.mainOptions setValue:[NSNull null] forKeyPath:@"request.slice.maxStops"];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (void)addOptions:(UIButton *)sender {
    AdditionalOptionsViewController *additionalOptionViewController = [[AdditionalOptionsViewController alloc] init];
    additionalOptionViewController.delegate = self;
    [self.navigationController pushViewController:additionalOptionViewController animated:YES];
    [self.view endEditing:YES];
}

- (void)getResult:(UIButton *)sender {
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [self.mainView.indicatorView startAnimating];
    [[QPXService sharedService] getResult:self.mainOptions withComplition:^(NSArray * result){
        ResultViewController *resultViewController = [[ResultViewController alloc] init];
        dispatch_async(dispatch_get_main_queue(),^{
            [self.mainView.indicatorView stopAnimating];
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            if (!([result count] == 0)) {
                resultViewController.resultArray = result;
                [self.navigationController pushViewController:resultViewController animated:YES];
                [self.view endEditing:YES];
            }
            else {
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:nil
                                             message:@"По заданным параметрам рейсов не найдено!"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"ОК"
                                            style:UIAlertActionStyleDefault
                                            handler:nil];
                
                [alert addAction:yesButton];
                [self presentViewController:alert animated:YES completion:nil];
            }
        });
    }];
    [self.mainOptions setValue:@"0" forKeyPath:@"request.passengers.childCount"];
    [self.mainOptions setValue:[NSNull null] forKeyPath:@"request.slice.preferredCabin"];
    [self.mainOptions setValue:[NSNull null] forKeyPath:@"request.maxPrice"];
    [self.mainOptions setValue:[NSNull null] forKeyPath:@"request.refundable"];
    [self.mainOptions setValue:[NSNull null] forKeyPath:@"request.slice.permittedDepartureTime.earliestTime"];
    [self.mainOptions setValue:[NSNull null] forKeyPath:@"request.slice.permittedDepartureTime.latestTime"];
}

- (void)getLiked:(UIBarButtonItem *)sender {
    LikedViewController *likedViewController = [[LikedViewController alloc] init];
    [self.navigationController pushViewController:likedViewController animated:YES];
    [self.view endEditing:YES];
}

- (void)setAirport:(NSString *)airport andCode:(NSString *)code {
    self.selectedTextField.text = airport;
    if (self.selectedTextField.tag == 1) [self.mainOptions setValue:code forKeyPath:@"request.slice.origin"];
    else [self.mainOptions setValue:code forKeyPath:@"request.slice.destination"];
}

- (void)updateOptionsWithValue:(NSString *)value forKeyPath:(NSString *)keyPath {
    [self.mainOptions setValue:value forKeyPath:keyPath];
    //NSLog(@"%@",self.mainOptions);
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.mainView.topOffset = self.topLayoutGuide.length;
}


@end
